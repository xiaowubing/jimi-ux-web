import UpperUser from './upperUser.vue';

UpperUser.install = function(Vue){
    Vue.component('UpperUser', UpperUser);
};

export default UpperUser;
