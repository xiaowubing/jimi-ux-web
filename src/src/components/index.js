/* 
 * @Author: xiaowubing 
 * @Date: 2021-03-10 15:11:29 
 * @Last Modified by: xiaowubing 
 * @Last Modified time: 2021-03-10 15:11:29 
 */
import UpperUser from './upperUser';

const components = [
    UpperUser,
];

const install = function(Vue) {
    components.forEach(component => {
        Vue.component(component.name, component);
    });
};

export default {
    install
};