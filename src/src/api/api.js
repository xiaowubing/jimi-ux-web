
import { get, post, put, patch, del } from '../http/http';

// const fileConfig = {
//     headers: {
//         'Content-Type': 'multipart/form-data'
//     }
// };

// const pushConfig = {
//     headers: {
//         'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
//     }
// };

export default {

    // 登录
    login: {
        login: (val) => post('/auth/login', val), // 登录
        logout: (val) => get('/auth/logout', val), // 登出
        register: (val) => post('/auth/reg', val), // 注册
        getCode: (val) => post('/auth/reg-code', val), // 验证码
        resetPass: (val) => post('/auth/resetPass', val), // 修改密码
    },

    // 推送配置
    push: {
        getList: (val) => post('/expr/list', val), // 查询列表
        addList: (val) => put('/expr/put', val), // 添加
        delList: (val) => del('/expr/del', val), // 删除
        putList: (val) => patch('/expr/update', val), // 编辑
        release: (val) => post('/expr/get', val),  // 详情
    },
  
};