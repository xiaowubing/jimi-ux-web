// 因为sessionStorage不能跨标签共享,token还是用localStorage

/** 
 * 存储localStorage
 * @param {String} key 存储的键
 * @param {Object} key 存储的数据
 */
export const saveLocalStorage = (key, value) => {
    if (JSON.stringify(value)) {
        localStorage.setItem(key, JSON.stringify(value));
    }
};

/** 
 * 存储sessionStorage
 * @param {String} key 存储的键
 * @param {Object} value 存储的数据
 */
export const saveSessionStorage = (key, value) => {
    if (JSON.stringify(value)) {
        sessionStorage.setItem(key, JSON.stringify(value));
    }
};

/** 
 * 获取本地存储
 * @param {String} key 本地存储的键
 */
export const getLocalStorage = (key) => {
    if (localStorage.getItem(key)) {
        return JSON.parse(localStorage.getItem(key));
    } else if (sessionStorage.getItem(key)) {
        return JSON.parse(sessionStorage.getItem(key));
    } else {
        return '';
    }
};

/** 
 * 删除本地存储
 * @param {String} key 本地存储的键
 */
export const removeLocalStorage = (key) => {
    localStorage.removeItem(key);
};

/** 
 * 全部删除本地存储
 */
export const clearLocalStorage = () => {
    localStorage.clear();
    sessionStorage.clear();
};

export default {
    saveLocalStorage,
    saveSessionStorage,
    getLocalStorage,
    removeLocalStorage,
    clearLocalStorage
};
