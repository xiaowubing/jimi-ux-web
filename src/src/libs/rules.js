let rules = {
    'required': (tip) => { // 必填(tip为提示语)
        return {
            required: true,
            message: tip,
        };
    },
    'chinese': { // 中文、英文、数字
        pattern: /^[\u4E00-\u9FA5A-Za-z\d]+$/,
        message: '只允许输入中文、英文、数字'
    },
    'all': (min, max, tip) => { // 任何字符
        return {
            pattern: new RegExp(`^[\\S]{${min},${max}}$`),
            message: tip || `${min}-${max}个字符`,
        };
    },
    'number': (min, max, tip) => { // 数字 (tip为提示语) 
        return {
            pattern: new RegExp(`^([0-9]){${min},${max}}$`),
            message: tip || `${min}-${max}个字符, 仅限数字`,
        };
    },
    'space': () => {
        return {
            pattern: /^[^\s]*$/,
            message: '不允许输入空格'
        };
    },
    'twoDecimal': { // 两位小数    
        pattern: /^\d+(\.\d{0,2})?$/,
        message: '只允许数字,小数点后仅支持两位'
    },
    'positiveInteger': (tip) => { // 包括0整数
        return {
            pattern: /^(\d)+$/,
            message: tip || '只允许正整数'
        };
    },
    'noZeroInteger': (tip) => { // 不包括0正整数
        return {
            pattern: /^[1-9](\d)*$/,
            message: tip || '只允许正整数'
        };
    },
    'password': { // 密码格式
        pattern: /^([a-zA-Z0-9\@\*\.\#]){6,18}$/,
        message: '密码格式错误',
        trigger: 'blur'
    },
    'email': { // 邮箱格式
        pattern: /^[A-Za-z0-9]+([._\\-]*[A-Za-z0-9])*@([A-Za-z0-9]+[-A-Za-z0-9]*[A-Za-z0-9]+.){1,63}[A-Za-z0-9]+$/,
        message: '请输入正确的邮箱',
        trigger: 'blur'
    },
    'phone': {
        pattern: /^((0\d{2,3}\d{7,8})|((13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}))$/,
        message: '号码格式不正确',
        trigger: 'blur'
    },
    'telphone': {
        pattern: /^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/,
        message: '请输入正确的手机号',
        trigger: 'blur'
    },
    'card': {
        pattern: /^[\d]{17}[\dXx]$/,
        message: '证件号码格式不正确',
        trigger: 'blur'
    }
};

export default {
    /**
     * 登录
     */
    login: () => {
        return {
            'name': [ // 名称
                rules.required('请输入用户名'), // 必填
            ],
            'password': [ // 名称
                rules.required('请输入密码'), // 必填
            ],
        };
    },
    
};