import axios from 'axios';

import Vue from 'vue';

let vue = Vue.prototype;

import qs from 'qs';

import store from '../store';

import router from '../router/index';

axios.defaults.timeout = 30 * 1000; // 响应时间
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'; // 配置请求头

/**
 * 环境的切换 - 配置接口地址
 */
if (process.env.NODE_ENV === 'development') { 
    axios.defaults.baseURL = '/api'; // 开发地址
} else if (process.env.NODE_ENV === 'production') {
    axios.defaults.baseURL = window.jmConfig.baseURL; // 线上地址
}

/**
 * 请求拦截器
 */
axios.interceptors.request.use(config => {

    if (store.state.login.token) { // 判断是否存在token，如果存在的话，则每个http header都加上token
        config.headers.Authorization = 'Bearer ' + store.state.login.token;
    }
    if (config.headers['Content-Type'] === 'application/x-www-form-urlencoded;charset=UTF-8') {
        config.data = qs.stringify(config.data, {
            arrayFormat: 'repeat'
        }); // 序列化请求参数;
    }
    return config;

}, err => {
   
    vue.$uxLoading?.hide();
    return Promise.reject(err);
});

/**
 * 响应拦截器
 */
axios.interceptors.response.use(res => {
   
    vue.$uxLoading?.hide();

    if (res.data.code !== 10000 && !res.config.config) {
      
        vue.$uxMessage?.error(`${res.data.sub_msg || res.data.msg}`);
    }
    if (res.data.code === 20001) {
      
        vue.$uxModal?.hide();
        router.replace({path: '/login'});
    }
    return res;

}, err => {
   
    vue.$uxLoading?.hide();
    if (err.response?.data.code === 20001) {
        vue.$uxModal?.hide();
        router.replace({path: '/login'});
    }
    vue.$uxMessage?.error(err.response ? err.response.data.sub_msg || '出错了, 请稍后再试' : '出错了, 请稍后再试');
    return Promise.reject(err);
});

export default axios;

/** 
 * get方法
 * @param {String} url 请求地址
 * @param {Object} params 请求参数
 * @param {Object} config 请求的配置
 */
export function get(url, params, config) {
    let time = new Date().getTime();
    return new Promise((resolve, reject) => {
        axios.get(`${url}?time=${time}`, {
            params: params,
            config: config
        }).then(res => {
            resolve(res.data, res);
        }).catch(err => {
            reject(err);
        });
    });
}

/** 
 * post方法
 * @param {String} url 请求地址
 * @param {Object} params 请求参数
 * @param {Object} config 请求的配置
 */
export function post(url, params, config) {
    return new Promise((resolve, reject) => {
        axios.post(url, params, config).then(res => {
            resolve(res.data, res);
        }).catch(err => {
            reject(err);
        });
    });
}

/** 
 * put方法
 * @param {String} url 请求地址
 * @param {Object} params 请求参数
 * @param {Object} config 请求的配置
 */
export function put(url, params, config) {
    return new Promise((resolve, reject) => {
        axios.put(url, params, config).then(res => {
            resolve(res.data, res);
        }).catch(err => {
            reject(err);
        });
    });
}

/** 
 * patch方法
 * @param {String} url 请求地址
 * @param {Object} params 请求参数
 * @param {Object} config 请求的配置
 */
export function patch(url, params, config) {
    return new Promise((resolve, reject) => {
        axios.patch(url, params, config).then(res => {
            resolve(res.data, res);
        }).catch(err => {
            reject(err);
        });
    });
}

/** 
 * delete方法
 * @param {String} url 请求地址
 * @param {Object} params 请求参数
 * @param {Object} config 请求的配置
 */
export function del(url, params, config) {
    return new Promise((resolve, reject) => {
        axios.delete(url, {
            params: params,
            config: config
        }).then(res => {
            resolve(res.data, res);
        }).catch(err => {
            reject(err);
        });
    });
}
