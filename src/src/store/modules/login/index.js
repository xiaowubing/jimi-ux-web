import { saveLocalStorage, getLocalStorage } from '@l/localStorage';

let login = {

    state: {
        token: getLocalStorage('token'), // 用户token
    },
    
    mutations: {

        /**
         * 保存-token
         * @param {Object} state 状态对象
         * @param {String} data token
         */
        saveToken(state, data) {
            saveLocalStorage('token', data);
            state.token = data;
        }
    },

    actions: {

    }
};

export {login};