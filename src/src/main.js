/* 
 * @Author: xiaowubing 
 * @Date: 2021-03-10 15:12:10 
 * @Last Modified by: xiaowubing 
 * @Last Modified time: 2021-03-10 15:12:10 
 */
import 'babel-polyfill';
import promise from 'es6-promise';
promise.polyfill();
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Cookies from 'js-cookie'; // cookie插件
import uxkit from 'jimi-uxkit/packages/index.js';
import 'jimi-uxkit/packages/styles/index.scss';

Vue.use(uxkit);

import './assets/css/base.css'; // 初始化样式
import axios from '@h/http'; // http请求
import api from '@a/api'; // api接口
import rules from '@l/rules.js'; // 验证规则

import Components from '@c'; // 自定义封装组件集合

Vue.use(Components);

Vue.prototype.$axios = axios;
Vue.prototype.$api = api;
Vue.prototype.$rul = rules;
Vue.prototype.$cookie = Cookies;
Vue.prototype.$uxSdk = uxkit.uxSdk;

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');