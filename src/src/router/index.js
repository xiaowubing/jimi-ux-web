import Vue from 'vue';

import Router from 'vue-router';

Vue.use(Router);

/** meta配置定义
 * requireAuth: 需要登录才可以访问
 * active: 当前路由激活的菜单
 * noAuth: 不需要菜单授权, 登录后可直接访问
 * bread: 面包屑数据
 */
const router = new Router({
    routes: [
        {
            path: '',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'login',
            component: resolve => require(['@v/login/login'], resolve), // 首页
        },
        {
            path: '/home',
            name: 'home',
            component: resolve => require(['@v/home/home'], resolve), // 首页
            meta: {requireAuth: true, noAuth: true, active: 'home', bread: [{ name: '主页', icon: 'ux_icon_backstage'}]}
        },
        {
            path: '/404',
            name: '404',
            component: resolve => require(['@v/warning/error'], resolve)
        },
        {
            path: '/error',
            name: 'error',
            component: resolve => require(['@v/warning/error'], resolve)
        },
        {
            path: '*',
            component: resolve => require(['@v/warning/error'], resolve)
        },
    ]
});

router.beforeEach((to, from, next) => {
    Vue.prototype.$uxModal.hide();
    Vue.prototype.$uxLoading.hide();
    next();    
});

export default router;