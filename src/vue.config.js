const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '././' : '',
    devServer: {
        port: 9521,
        proxy: {
            '/api': {
                target: 'http://172.26.10.45:7777/back-end/',
                changeOrigin: true,
                ws: true,
                pathRewrite: { // 需要rewrite重写的, 如果在服务器端做了处理则可以不要这段
                    '^/api': ''
                }
            }
        }
    },
    chainWebpack: config => {
        // 设置别名
        config.resolve.alias
            .set('@', resolve('src'))
            .set('@a', resolve('src/api'))
            .set('@c', resolve('src/components'))
            .set('@h', resolve('src/http'))
            .set('@l', resolve('src/libs'))
            .set('@r', resolve('src/router'))
            .set('@s', resolve('src/store'))
            .set('@v', resolve('src/views'));
    },
    css: {
        extract: process.env.NODE_ENV === 'production',
        loaderOptions: {
            scss: {
                // 全局无需引入直接使用
                prependData: `
                    @import "~@/assets/scss/index.scss";
                `
            }
        },
        sourceMap: true,
    },
    transpileDependencies: ['jimi-uxkit', 'element-ui'],
    productionSourceMap: true,
};