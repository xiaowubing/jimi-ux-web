# 控制台

+ [正式环境](http://console.jimicloud.com/)
+ [预发环境](http://console.jimicloud.com/)
+ [测试环境](http://172.26.10.45:7777)

测试环境体验账号: admin 密码: jimi@123

## 警告
```
1. 如发现Eslint格式验证错误，请改正之后进行提交！！！

2. 请先合并完成之后，启动服务进行验证，再push到线上地址！！！

3. 提交到本地仓库时请详细写明本次提交修改功能！！！
```

## Build Setup

```bash
#### install dependencies
npm install

#### serve with hot reload at localhost:8080
npm run dev

#### build for production with minification
npm run build

```

## 项目说明

### 关于框架
1. [vuex](https://vuex.vuejs.org/zh/)
2. [js-cookie](https://www.npmjs.com/package/js-cookie)
3. [axios](https://vuex.vuejs.org/zh/)
4. [qs](https://vuex.vuejs.org/zh/)
5. [element-ui](http://element-cn.eleme.io/#/zh-CN/component/table)
6. [lodashjs](https://www.lodashjs.com/)
6. [jimi-uxkit](http://172.26.10.45:8888/jimi-uxkit/#/introduce)

### 关于功能
- 登录  * 登录时密码采用一次MD5加密
- 应用管理
  - 应用列表
  - 回收列表
- 额度管理
- 产品总览
- 接口服务
  - API分组
  - API列表
  - 流量控制
  - 访问策略
  - API Code
  - 产品管理
  - 调试工具
- 用量统计
  - 调用统计
- 设备服务
  - 设备接入
  - 设备日志
  - 心跳日志
- 账号列表
- 个人中心

### 关于axios
axios4种请求方法已封装,按照原有格式定义请求方法、请求地址、请求参数后，导出即可。
如需设置请求头以及其他特殊情况可使用 this.$axios.post 方式调用

### 关于样式
1. 饿了吗样式通过官网配置主题色(#4880ff)下载到本地。
2. [初始化样式](src/assets/css/base.css)定义标签初始化样式,以及修改了部分饿了吗样式(主要修复一些效果, 不影响其他)
3. [全局快捷样式](src/assets/scss/public.scss)类名以x-开头为自定义快捷样式,方便布局。
4. [弹窗样式](src/assets/scss/modal.scss)模态框样式。
5. 页面样式使用scss编写，使用私有样式（即scoped属性）

### 关于路由
1. requireAuth: true 表示需要登录才可访问
2. active: 当前路由激活的菜单
3. noAuth: 表示无需与后台返回的权限比对, 登录后可直接访问
4. bread: [{ name: '应用列表', icon: 'icongongzuotai', url: '/applyList' }, { name: '应用详情' }]，表示当前页面的面包屑数据, 注意url为'-1'时, 即可实现返回上一页

### 关于组件
1. 封装通用组件统一在前面加my来区分, 封装组件统一按照原有格式书写,

### 关于路径
1.  @  等比[src](src)
2.  @a 等比[api](src/api)
3.  @c 等比[components](src/components)
4.  @l 等比[libs](src/libs)
5.  @s 等比[store](src/store)
6.  @r 等比[router](src/router)


### 关于目录
```
├─ doc 开发设计文档
├─ eff 原型&效果图
├─ req 项目需求文件
└─ src 项目源码
    |-- public
    |-- src
        |-- App.vue
        |-- main.js
        |-- api
        |   |-- api.js //存放所有的请求
        |-- assets //存放公共文件
        |   |-- css	
        |   |   |-- base.css //公共根样式
        |   |   |-- element
        |   |       |-- index.css
        |   |       |-- fonts
        |   |           |-- element-icons.ttf
        |   |           |-- element-icons.woff
        |   |-- images
        |   |   |-- login
        |   |   |   |-- bcg.png
        |   |   |-- monitorCenter
        |   |   |   |-- end.png
        |   |   |   |-- point.png
        |   |   |   |-- selected.png
        |   |   |   |-- start.png
        |   |   |   |-- unchecked.png
        |   |   |-- web
        |   |       |-- 404.png
        |   |       |-- 500.png
        |   |       |-- blank.png
        |   |-- scss
        |       |-- index.scss
        |       |-- public.scss
        |-- components
        |   |-- index.js
        |   |-- render.js
        |   |-- area
        |   |   |-- city2.js
        |   |   |-- index.js
        |   |   |-- myArea.vue
        |   |-- breadcrumb
        |   |   |-- breadcrumb.vue
        |   |   |-- index.js
        |   |-- checkboxGroup
        |   |   |-- checkboxGroup.vue
        |   |   |-- index.js
        |   |-- editPassword
        |   |   |-- editPassword.vue
        |   |   |-- index.js
        |   |-- footerBtn
        |   |   |-- footerBtn.vue
        |   |   |-- index.js
        |   |-- formItem
        |   |   |-- formItem.vue
        |   |   |-- index.js
        |   |-- headTitle
        |   |   |-- headTitle.vue
        |   |   |-- index.js
        |   |-- iconImg
        |   |   |-- iconImg.vue
        |   |   |-- index.js
        |   |-- imgList
        |   |   |-- imgList.vue
        |   |   |-- index.js
        |   |-- infoItrm
        |   |   |-- index.js
        |   |   |-- infoItem.vue
        |   |-- leftMenu
        |   |   |-- index.js
        |   |   |-- leftMenu.vue
        |   |-- loading
        |   |   |-- index.js
        |   |   |-- loading.vue
        |   |   |-- readMe.md
        |   |-- map
        |   |   |-- index.js
        |   |   |-- map.vue
        |   |-- menu
        |   |   |-- index.js
        |   |   |-- menu.vue
        |   |-- message
        |   |   |-- index.js
        |   |   |-- message.vue
        |   |-- organizeTree
        |   |   |-- index.js
        |   |   |-- organizeTree.vue
        |   |-- radioGroup
        |   |   |-- index.js
        |   |   |-- radioGroup.vue
        |   |-- realTimeAlarm
        |   |   |-- index.js
        |   |   |-- realTimeAlarm.vue
        |   |-- seachOption
        |   |   |-- index.js
        |   |   |-- seachOption.vue
        |   |-- select
        |   |   |-- index.js
        |   |   |-- select.vue
        |   |-- showModal
        |   |   |-- index.js
        |   |   |-- showModal.vue
        |   |-- table
        |   |   |-- addText.vue
        |   |   |-- index.js
        |   |   |-- render.js
        |   |   |-- table.vue
        |   |-- tableBtn
        |   |   |-- index.js
        |   |   |-- tableBtn.vue
        |   |-- tableDot
        |   |   |-- index.js
        |   |   |-- tableDot.vue
        |   |-- tableTag
        |   |   |-- index.js
        |   |   |-- tableTag.vue
        |   |-- tipInfo
        |   |   |-- index.js
        |   |   |-- tipInfo.vue
        |   |-- video
        |       |-- index.js
        |       |-- video.vue
        |-- http
        |   |-- http.js //统一请求和响应方法封装
        |-- libs
        |   |-- localStorage.js //操作本地缓存的封装
        |   |-- menu.js //根据权限封装菜单栏
        |   |-- method.js //JSON数据格式化展示
        |   |-- options.js //用于页面各种选择类, 以及表格中数字转义
        |   |-- order.js
        |   |-- rules.js //校验规则的封装
        |-- router
        |   |-- index.js //路由表
        |-- store 
        |   |-- index.js //主仓库
        |   |-- modules //仓库进行模块化
        |       |-- api
        |       |   |-- index.js
        |       |-- login
        |           |-- index.js
        |-- views
            |-- home.vue //主体内容页面
            |-- apiServices //接口服务页面
            |   |-- access.vue	//访问策略页面
            |   |-- accessInfo.vue	//访问策略详情页
            |   |-- apiCode.vue	//Api Code页面
            |   |-- apiInfo.vue	//Api列表详情页
            |   |-- apiList.vue	//Api列表页面
            |   |-- flowInfo.vue	//流量控制页面
            |   |-- flowList.vue	//流量控制详情页
            |   |-- template
            |       |-- addAccess.vue	//访问策略新增弹窗
            |       |-- addAccessApi.vue	//访问策略详情页添加API弹窗
            |       |-- addApi.vue	//API列表新增界面
            |       |-- addCode.vue	//Api Code新增弹窗
            |       |-- addFlow.vue	//流量控制新增弹窗
            |       |-- addPlugin.vue	
            |       |-- bindAccess.vue	//Api列表发布弹窗
            |       |-- bindAccessIp.vue	
            |       |-- bindApi.vue	//访问策略详情页添加IP弹窗
            |       |-- bindApply.vue	
            |       |-- bindFlowApi.vue	//访问策略详情绑定API弹窗
            |       |-- bindFlowApp.vue	//流量控制详情页绑定应用弹窗
            |       |-- release.vue	//API列表发布下架弹窗
            |       |-- swagger.vue	//API列表swagger导入页面
            |       |-- unbindApply.vue	
            |       |-- unbindFlowApp.vue	
            |       |-- apiStepFour	//添加API第四步
            |       |   |-- addFieldInput.vue
            |       |   |-- apiStepFour.vue	//添加API第四步页面
            |       |-- apiStepOne	//添加API第一步
            |       |   |-- apiStepOne.vue	//添加API第一步页面
            |       |-- apiStepThree	//添加API第三步
            |       |   |-- addFieldInput.vue	//后端服务参数配置列表
            |       |   |-- addParamInput.vue	//固定参数列表
            |       |   |-- apiStepThree.vue	//添加API第三步页面
            |       |-- apiStepTwo	//添加API第二步
            |           |-- addFieldInput.vue	//入参定义列表
            |           |-- apiStepTwo.vue	//添加API第二步页面
            |-- apply	//应用管理
            |   |-- applyInfo.vue	//应用列表详情
            |   |-- applyList.vue	//应用列表页面
            |   |-- recoveryList.vue
            |   |-- template
            |       |-- addApply.vue	//新增弹窗
            |       |-- approval.vue
            |       |-- bindProduct.vue
            |       |-- passDot.vue
            |       |-- unbindApi.vue
            |-- debugg	//调试工具
            |   |-- debug1.vue	//	管理员账号调试页面
            |   |-- debug2.vue	//客户子账号调试页面
            |-- group	//API分组
            |   |-- groupInfo.vue	//API分组详情
            |   |-- groupList.vue //API分组列表
            |   |-- template
            |       |-- addGroup.vue	//新增弹窗
            |       |-- certiFicate.vue	//证书
            |       |-- domain.vue	//绑定域名
            |       |-- editEnv.vue	//更换域名
            |       |-- envShow.vue
            |       |-- evnDomains.vue
            |       |-- groupDeital.vue
            |-- login
            |   |-- firstPage.vue	
            |   |-- forgetPassword.vue	//忘记密码
            |   |-- login.vue	//控制台登录页面
            |   |-- register.vue //注册
            |   |-- resetPass.vue //重置密码
            |-- manage	//设备服务
            |   |-- deviceAccess.vue	//设备接入
            |   |-- template
            |       |-- addDevice.vue	//设备接入页面新增弹窗
            |       |-- devDomains.vue	//
            |-- manageProduct
            |-- myline
            |   |-- myLine.vue //额度管理
            |   |-- template
            |       |-- showQuota.vue	
            |       |-- topUp.vue //额度充值弹窗
            |-- product //产品管理
            |   |-- generateApi.vue //生成API文档页面
            |   |-- productAll.vue	//产品总览
            |   |-- productAllInfo.vue	//产品总览详情
            |   |-- productInfo.vue	//产品详情
            |   |-- productList.vue	//产品列表展示
            |   |-- template
            |       |-- addApi.vue //添加API弹窗
            |       |-- addProduct.vue //新增弹窗
            |-- service
            |   |-- service.vue
            |-- statistics
            |   |-- deviceLog.vue	//设备日志
            |   |-- heartLog.vue	//心跳日志
            |   |-- invoking.vue	//调用统计
            |-- template
            |   |-- editPassword
            |   |   |-- editPassword.vue
            |   |   |-- index.js
            |   |-- timeButton
            |   |   |-- index.js
            |   |   |-- timeButton.vue
            |   |-- upperUser
            |       |-- index.js
            |       |-- upperUser.vue
            |-- userList
                |-- personal.vue //账号列表新增弹窗
                |-- userList.vue //账号列表
                |-- template
                    |-- addUser.vue
                    |-- bindEmail.vue
```