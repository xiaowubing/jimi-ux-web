window.jmConfig = {
    production: 'http://console.jimicloud.com/back-end', // 生产环境
};

// IE兼容
if (!!window.ActiveXObject || 'ActiveXObject' in window) {
    var d = document.createElement('div');
    d.style.cssText = 'position: fixed;top: 0;left: 0;z-index: 9999;width: 100%;height: 100%;background: #fff';
    d.innerHTML = '<div style="position:absolute;top:50%;left:0;width:100%;height:200px;margin-top:-100px;font-size:16px;line-height:200px;text-align:center;background: #fff;color:#4880ff;margin-bottom:40px;">您的浏览器版本太低，建议升级版本再使用，或者使用其他浏览器打开！ <a target="_blank" href="https://pc.qq.com/category/c3.html" style="background-color:#4880ff;border-color: #4880ff;text-decoration: none;padding: 6px 12px;background-image: none;border: 1px solid transparent;border-radius: 4px;font-size:16px;color:#fff;">立即升级</a></div>';
    var s = document.getElementsByTagName('body')[0];
    s.insertBefore(d, s.firstChild);
}