# 项目名称

+ [测试环境](###)

+ [正式环境](###)

## 注意事项
```
1. 如发现Eslint格式验证错误，请改正之后进行提交！！！

2. 请先合并完成之后，启动服务进行验证，再push到线上地址！！！

3. 提交到本地仓库时请详细写明本次提交修改功能！！！
```

## Build Setup

```bash
#### install dependencies
npm install

#### serve with hot reload at localhost:8092
npm run dev

#### build for production with minification
npm run build

```

## 项目说明

### 关于框架
+ [vuex](https://vuex.vuejs.org/zh/)
+ [axios](https://vuex.vuejs.org/zh/)
+ [element-ui](https://element.eleme.cn/#/zh-CN/component/installation)
+ [jimi-uxkit](http://172.26.10.45:8888/jimi-uxkit/#/introduce)


### 关于功能
- 登录
- 首页
- 数据管理
- 设备管理
- 用户管理
    - 用户列表
    - 群组列表

### 关于axios
axios4种请求方法已封装,按照原有格式定义请求方法、请求地址、请求参数后，导出即可。
如需设置请求头以及其他特殊情况可使用 this.$axios.post 方式调用

### 关于样式
+ 页面样式使用scss编写，使用私有样式（即scoped属性）

### 关于路由
+ requireAuth: true 表示需要登录才可访问
+ active: 当前路由激活的菜单
+ noAuth: 表示无需与后台返回的权限比对, 登录后可直接访问

### 关于组件
+ 封装通用组件统一在前面加my来区分, 封装组件统一按照原有格式书写,

### 关于路径
+ @  等比[src](src)
+ @a 等比[api](src/api)
+ @c 等比[components](src/components)
+ @h 等比[http](src/components)
+ @l 等比[libs](src/libs)
+ @r 等比[router](src/router)
+ @s 等比[store](src/store)
+ @v 等比[views](src/views)

### 关于目录
```
├─ doc // 开发设计文档
├─ eff // 原型&效果图
├─ req // 项目需求文件
└─ src // 项目源码
   ├─ public
   │  ├─ static
   │  └─ index.html
   ├─ src
   │  ├─ api
   │  ├─ assets
   │  ├─ components
   │  ├─ http
   │  ├─ libs
   │  ├─ router
   │  ├─ store
   │  └─ views
   │     ├─ login
   │     └─ home
```